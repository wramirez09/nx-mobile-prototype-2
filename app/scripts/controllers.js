'use strict';
angular.module('MobileAlpha12.controllers', [])

.controller('DashCtrl', function($scope, $state, $ionicModal) {

    var tag = angular.element('.listItem');
    tag.hide();


    $scope.showList = function() {
        console.log('list shown');
        var tag = angular.element('.listItem');
        tag.fadeIn('easeIn');
    };
    $scope.hideList = function() {
        console.log('hide list');
        var tag = angular.element('.listItem');
        tag.fadeOut(500);
    };

    $scope.gotTo = function(i) {
        $state.go(i);
    };

    // modal 

    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });
})

.controller('FriendsCtrl', function($scope, Friends) {
    $scope.friends = Friends.all();
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
    $scope.friend = Friends.get($stateParams.friendId);
})

.controller('AccountCtrl', function() {
    // $scope;
})

.controller('LoginCtrl', function($scope, $stateParams, $state) {
    $scope.login = {};

    $scope.loggedIn = function() {
        $state.go('tab.dash');
        console.log('logged in ');
    };
})


.controller('DetailsCtrl', function($scope, $ionicSlideBoxDelegate, $ionicModal, $state) {


   
  // hide stuff

    angular.element('.hiddenAcc').hide();
    
    var btnBar = angular.element('#button-bar');
    btnBar.hide();

    var compareUnit = angular.element('#compareUnit');
    compareUnit.hide();

 // drag and drop

    $scope.centerAnchor = true;
    $scope.toggleCenterAnchor = function() {
        $scope.centerAnchor = !$scope.centerAnchor
    }
    $scope.draggableObjects = [{
        'id': 1,
        'Name': 'St. Joseph Hospital',
        'Specialties': 'General Practice, Internal Medicine',
        'Address': '2900 N. Lake Shore , Chicago, IL',
        'Phone': '(312) 236-3175',
        'Distance': '0 Miles'
    }, {
        'id': 2,
        'Name': 'Chicago Northside MRI Center',
        'Specialties': 'Neurological Surgery',
        'Address': '1460 N. Halsted, Chicago, IL',
        'Phone': '(312) 477-2400',
        'Distance': '0.2 Miles'
    }, {
        'id': 3,
        'Name': 'Northwestern Memorial Hospital',
        'Specialties': 'Pediatrics',
        'Address': '251 E. Huron, Chicago, IL',
        'Phone': '(312) 255-9570',
        'Distance': '0.2 Miles'
    }];
    $scope.droppedObjects1 = [];
    $scope.droppedObjects2 = [];


    $scope.onDropComplete1 = function(data, evt) {
        // alert('dropped 1');
        var title1 = angular.element('#title1');
        title1.hide();
        var index = $scope.droppedObjects1.indexOf(data);
        if (index == -1)
            $scope.droppedObjects1.push(data);
    }
    $scope.onDragSuccess1 = function(data, evt) {
        // alert('dropped successfully');
        // console.log("133","$scope","onDragSuccess1", "", evt);
        var index = $scope.droppedObjects1.indexOf(data);
        if (index > -1) {
            $scope.droppedObjects1.splice(index, 1);
        }


    }
    $scope.onDragSuccess2 = function(data, evt) {
        // alert('dragged successfully');
        var index = $scope.droppedObjects2.indexOf(data);
        if (index > -1) {
            $scope.droppedObjects2.splice(index, 1);
        }
    }
    $scope.onDropComplete2 = function(data, evt) {
        // alert('dropped successfully');
        var index = $scope.droppedObjects2.indexOf(data);
        if (index == -1) {
            $scope.droppedObjects2.push(data);
        }


    }
    var inArray = function(array, obj) {
        var index = array.indexOf(obj);
    };





    // end drag and drop 

    // small data set for poto

    $scope.hosp = [{
        'id': 1,
        'Name': 'St. Joseph Hospital',
        'Specialties': 'General Practice, Internal Medicine',
        'Address': '2900 N. Lake Shore , Chicago, IL',
        'Phone': '(312) 236-3175',
        'Distance': '0 Miles'
    }, {
        'id': 2,
        'Name': 'Chicago Northside MRI Center',
        'Specialties': 'Neurological Surgery',
        'Address': '1460 N. Halsted, Chicago, IL',
        'Phone': '(312) 477-2400',
        'Distance': '0.2 Miles'
    }, {
        'id': 3,
        'Name': 'Northwestern Memorial Hospital',
        'Specialties': 'Pediatrics',
        'Address': '251 E. Huron, Chicago, IL',
        'Phone': '(312) 255-9570',
        'Distance': '0.2 Miles'
    }, {
        'id': 4,
        'Name': 'MRI of River North',
        'Specialties': 'Neurological Surgery',
        'Address': '559 W. Kenzie, Chicago, IL',
        'Phone': '(312) 477-2400',
        'Distance': '0.2 Miles'
    }, {
        'id': 5,
        'Name': 'MRI Lincoln Imaging Center',
        'Specialties': 'Hospitalist, Internal Medicine',
        'Address': '1228 W. Belmont, Chicago, IL',
        'Phone': '(312) 926-5924',
        'Distance': '0.2 Miles'
    }, {
        'id': 6,
        'Name': 'Streeterville Open MRI',
        'Specialties': 'Urology',
        'Address': '446 E. Ontario #106, Chicago, IL',
        'Phone': '(312) 595-1444',
        'Distance': '0.2 Miles'
    }];


    //sliders 

var divAcc = angular.element('.hiddenAcc');

divAcc.hide();


    $scope.reveal = function(IdOfDiv, IdofBtn) {

        var div = angular.element(IdOfDiv);
        var btn = angular.element(IdofBtn);

        if (btn.hasClass('ion-chevron-up')) {
            btn.removeClass('ion-chevron-up');
            btn.addClass('ion-chevron-down');
        } else {
            btn.removeClass('ion-chevron-down');
            btn.addClass('ion-chevron-up');
        }

        div.slideToggle();

    };



    $scope.showCompare = function(compareUnit){
        // alert('test');
        var compUnit = angular.element('#compareUnit');
        compUnit.slideToggle();
    };



    //slider
    $scope.next = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
        $ionicSlideBoxDelegate.previous();
        angular.element(this).hide();
    };
    $scope.slideTo = function(i, $index) {
        $ionicSlideBoxDelegate.slide(i);
    };

    $scope.slideHasChanged = function($index) {
        var btnBar = angular.element('#button-bar');

        console.log(compareUnit);
        if ($index == 1) {
            // alert('location slide');
            var btnBar = angular.element('#button-bar');
            btnBar.slideDown();
            var compareUnit = angular.element('#compareUnit');
            compareUnit.hide();

        }

        else if($index == 0){
          angular.element('.hiddenAcc').hide();
          btnBar.slideUp();
        }

         else {
            btnBar.slideUp();
            dropBoxes.slideUp();
        }
    }


    // search     
    var tag = angular.element('.listItem');
    tag.hide();

    $scope.showList = function() {
        console.log('list shown');
        var tag = angular.element('.item-input-inset');
        tag.fadeIn(1000);
    };
    $scope.hideList = function() {
        console.log('hide list');
        var tag = angular.element('.listItem');
        tag.fadeOut(500);
    };




    // cost modal 
    $ionicModal.fromTemplateUrl('templates/cost-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });


       // loc modal 
    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.locModal = modal;
    });
    $scope.openLocModal = function() {
        $scope.locModal.show();
    };
    $scope.closeLocModal = function() {
        $scope.locModal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.locModal.remove();
    });
    // Execute action on hide modal
    $scope.$on('locModal.hidden', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('locModal.removed', function() {
        // Execute action
    });

    $scope.gotTo = function(i) {
        $state.go(i);

    };


    //verticle tabs


});
