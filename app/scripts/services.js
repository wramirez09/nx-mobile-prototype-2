'use strict';
angular.module('MobileAlpha12.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [
    { id: 0, name: 'Scruff McGruff' },
    { id: 1, name: 'G.I. Joe' },
    { id: 2, name: 'Miss Frizzle' },
    { id: 3, name: 'Ash Ketchum' }
  ];

  var hosp = [{
    'id': 1,
    'Name': 'Gregory Clarke, MD',
    'Specialties': 'General Practice, Internal Medicine',
    'Address': '333 N Michigan Ave, Ste 2014, Chicago, IL 60601',
    'Phone': '(312) 236-3175',
    'Distance': '0 Miles'
  }, {
    'id': 2,
    'Name': 'Jerrel Boyer, DO',
    'Specialties': 'Neurological Surgery',
    'Address': '160 E Illinois St, Chicago, IL 60611',
    'Phone': '(312) 477-2400',
    'Distance': '0.2 Miles'
  }, {
    'id': 3,
    'Name': 'Frederick M Cahan, MD',
    'Specialties': 'Pediatrics',
    'Address': '201 E Huron St Galter 12-260, Chicago, IL 60611',
    'Phone': '(312) 255-9570',
    'Distance': '0.2 Miles'
  }, {
    'id': 4,
    'Name': 'Herbert H Engelhard, III, MD',
    'Specialties': 'Neurological Surgery',
    'Address': '160 E Illinois St, Chicago, IL 60611',
    'Phone': '(312) 477-2400',
    'Distance': '0.2 Miles'
  }, {
    'id': 5,
    'Name': 'Kanan Gutgutia, MD',
    'Specialties': 'Hospitalist, Internal Medicine',
    'Address': '251 E Huron St Feinberg 16-738, Chicago, IL 60611',
    'Phone': '(312) 926-5924',
    'Distance': '0.2 Miles'
  }, {
    'id': 6,
    'Name': 'Steven G Koopman, MD',
    'Specialties': 'Urology',
    'Address': '160 E Illinois St, Chicago, IL 60611',
    'Phone': '(312) 595-1444',
    'Distance': '0.2 Miles'
  }];



  //

  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    },
    getLocs: function(locId) {
      // Simple index lookup
      return hosp[locId];
    }
  };
});
