'use strict';
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('MobileAlpha12', ['ionic', 'config', 'MobileAlpha12.controllers', 'MobileAlpha12.services', 'MobileAlpha12.directives', 'ngDraggable'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // setup an abstract state for the tabs directive
    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    // Each tab has its own nav history stack:

    .state('login', {
      url: '/login',
      templateUrl: 'templates/tab-login.html',
      controller: 'LoginCtrl'
    })

    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-dash.html',
          controller: 'DashCtrl'
        }
      }
    })

      .state('tab.doc-details', {
      url: '/doc-details',
      views: {
        'tab-dash': {
          templateUrl: 'templates/paula-ward.html',
          controller: 'DetailsCtrl'
        }
      }
    })

    .state('tab.loc-details', {
      url: '/loc-details',
      views: {
        'tab-dash': {
          templateUrl: 'templates/loc-details.html',
          controller: 'DetailsCtrl'
        }
      }
    })



    .state('tab.friends', {
      url: '/friends',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'FriendsCtrl'
        }
      }
    })

    .state('tab.medications', {
      url: '/medications',
      views: {
        'tab-meds': {
          templateUrl: 'templates/tab-medications.html',
          controller: 'FriendDetailCtrl'
        }
      }
    })

    .state('tab.cost', {
      url: '/cost',
      views: {
        'tab-cost': {
          templateUrl: 'templates/tab-cost.html',
          controller: 'DetailsCtrl'
        }
      }
    })

    .state('tab.help', {
      url: '/help',
      views: {
        'tab-help': {
          templateUrl: 'templates/tab-help.html',
          controller: 'DetailsCtrl'
        }
      }
    })

   .state('tab.details', {
      url: '/details',
      views: {
        'tab-dash': {
          templateUrl: 'templates/details.html',
          controller: 'DetailsCtrl'
        }
      }
    })

   .state('tab.compared', {
      url: '/compared',
      views: {
        'tab-dash': {
          templateUrl: 'templates/compare-locations.html',
          controller: 'DetailsCtrl'
        }
      }
    })

    .state('details.id', {
      url: '/details/:locId',
      templateUrl: 'templates/loc-details.html',
      controller: 'DetailsCtrl'
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});



  
